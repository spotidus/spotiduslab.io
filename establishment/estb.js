var data = $fuwuProxy()
data.loaded = false
data.page = "home"

onInloadsCompleted.push(() => data.loaded = true)

setTimeout(() => {
    const params = new URLSearchParams(location.search)
    if (params.has("initToken")){
        const token = params.get("initToken")
        history.replaceState({}, "", location.origin + location.pathname)

        const p = fetch(backendUrl + "/establishment/initial_token", { credentials: "include", method: "POST", body: token })
        p.then((r) => {
            if (!r.ok) throw "Not okay"
            data.page = "init"
        }).catch(readyNoToken)
    } else if (params.has("state") && params.get("state") == sessionStorage.getItem("spotify_oauth_state")) {
        const code = params.get("code")
        if (code != null){
            data.page = "loading"
            
            const body = {
                code: code,
                redirectUri: sessionStorage.getItem("spotify_oauth_uri")
            }

            fetch(backendUrl + "/establishment/oauth_code", { credentials: "include", method: "POST", body: JSON.stringify(body) }).then((r) => {
                if (!r.ok) throw "Not Okay"
                fullReady()
            }).catch(connectionError)
        } else {
            spotifyError()
        }

        sessionStorage.removeItem("spotify_oauth_state")
        sessionStorage.removeItem("spotify_oauth_uri")
        history.replaceState({}, "", location.origin + location.pathname)
    } else {
        checkReadiness()
    }
})

function checkReadiness(){
    const p = fetch(backendUrl + "/establishment/readiness", { credentials: "include" })
    p.then(async (r) => {
        const readiness = await r.text()
        if (!r.ok || readiness == "invalid"){
            readyNoToken()
        } else if (readiness == "init"){
            readyInit()
        } else if (readiness == "oauth"){
            readyOauth()
        } else if (readiness == "ready") {
            fullReady()
        }
    }, connectionError)
}

const readyInit = () => data.page = "init"
const readyOauth = () => data.page = "oauth"
const fullReady = () => data.page = "home"
const readyNoToken = () => data.page = "no-token"

const connectionError = (e) => console.log(e)
const spotifyError = (e) => console.log(e)

function validateInputs(force){
    const nameInput = document.querySelector("input[name=name]")

    let valid = nameInput.validity
    let parent = nameInput.parentElement

    let validCount = 0

    if (valid.valid){
        parent.removeAttribute("validation-error")
        validCount++
    } else if (valid.tooLong){
        parent.setAttribute("validation-error", "Zu lang")
    } else if ((valid.tooShort || valid.valueMissing) && (nameInput.value.length > 0 || force)){
        parent.setAttribute("validation-error", "Zu kurz")
    }

    const loginInput = document.querySelector("input[name=login]")
    valid = loginInput.validity
    parent = loginInput.parentElement
    if (valid.valid){
        parent.removeAttribute("validation-error")
        validCount++
    } else if (valid.tooLong){
        parent.setAttribute("validation-error", "Zu lang")
    } else if ((valid.tooShort || valid.valueMissing) && (loginInput.value.length > 0 || force)){
        parent.setAttribute("validation-error", "Zu kurz")
    } else if (valid.patternMismatch){
        parent.setAttribute("validation-error", "Nur Ziffern und Kleinbuchstaben sind erlaubt")
    }

    const pwInput = document.querySelector("input[name=password]")
    valid = pwInput.validity
    parent = pwInput.parentElement
    if (valid.valid){
        parent.removeAttribute("validation-error")
        validCount++
    } else if ((valid.tooShort || valid.valueMissing) && (pwInput.value.length > 0 || force)){
        parent.setAttribute("validation-error", "Zu kurz")
    }

    const pwRepeatInput = document.querySelector("input[name=password-repeat]")
    parent = pwRepeatInput.parentElement
    if (pwRepeatInput.value == pwInput.value){
        parent.removeAttribute("validation-error")
        validCount++
    } else if (pwRepeatInput.value.length > 0 || force) {
        parent.setAttribute("validation-error", "Passwörter stimmen nicht überein")
    }

    return validCount == 4
}

function submitInit(){
    const isValid = validateInputs(true)

    if (isValid){
        const body = {
            login: document.querySelector("input[name=login]").value,
            name: document.querySelector("input[name=name]").value,
            password: document.querySelector("input[name=password]").value
        }

        const p = fetch(backendUrl + "/establishment/initialize", { credentials: "include", method: "POST", body: JSON.stringify(body) })
        p.then((r) => {
            if (!r.ok) throw "Not Okay"
            readyOauth()
        }).catch(() => {
            checkReadiness()
        })
    }
}


function performSpotifyOauth(){
    const p = fetch(backendUrl + "/establishment/oauth_config", { credentials: "include" })
    p.then(async (r) => {
        if (!r.ok) throw "Not Okay"
        
        const state = crypto.randomUUID()
        const redirectUri = location.origin + location.pathname
        sessionStorage.setItem("spotify_oauth_state", state)
        sessionStorage.setItem("spotify_oauth_uri", redirectUri)

        const oauthConfig = await r.json()
        const params = new URLSearchParams({
            response_type: "code",
            client_id: oauthConfig.clientId,
            scope: oauthConfig.scope,
            redirect_uri: redirectUri,
            state: state,
            show_dialog: true
        })

        location.href = "https://accounts.spotify.com/authorize?" + params

    }).catch(connectionError)
}