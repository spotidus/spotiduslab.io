const onInloadsCompleted = [];

(function(){

    const inloadPromises = []
    let fulfilledId = -1

    let domReady = document.readyState.match(/interactive|complete/)

    document.addEventListener("DOMContentLoaded", () => { domReady = true; checkReadyness() });

    let callbackExecuted = false

    function checkReadyness(){
        setTimeout(() => {
            if (!callbackExecuted && fulfilledId == inloadPromises.length -1){
                callbackExecuted = true
                for (const callback of onInloadsCompleted) {
                    callback()
                }
            }
        }, 100)
    }

    function nextInload(element, href, tagname){
        const promiseId = inloadPromises.length
        const promise = Promise.all([...inloadPromises]).then(
            fetch(href).then((result) => result.text()).then((text) => {
                if (tagname == null){
                    const el = document.createElement("div")
                    el.innerHTML = text
                    element.replaceWith(...el.childNodes)
                } else {
                    const el = document.createElement(tagname)
                    el.innerHTML = text
                    element.replaceWith(el)
                }
                
                fulfilledId = promiseId
                checkReadyness()
            })
        )
        inloadPromises.push(promise)
    }

    const observerOptions = [document.head, { childList: true, subtree: true }]

    new MutationObserver(mutationList=>{

        for (const mutation of mutationList) {
            for (const node of (mutation.addedNodes ?? [])) {
                if (node.nodeName == "LINK" && node.matches("[href][rel='custom-inload']")){
                    const href = node.getAttribute("href")
                    nextInload(node, href, node.getAttribute("tag-name"))
                }
            }
        }
    }).observe(...observerOptions)
})()